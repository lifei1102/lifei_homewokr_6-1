package com.lagou.consumer;

import com.lagou.domain.Order;
import com.lagou.repository.OrderRepository;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class MyMessageListener {

    @Autowired
    private OrderRepository orderRepository;

    @RabbitListener(queues = "q.pay.dlx")
    public void getMyMessage(@Payload String message){
        System.out.println(message);
        Integer orderId = Integer.parseInt(message.split("_")[1]);
        Optional<Order> byId = orderRepository.findById(orderId);
        if(byId.isPresent()) {
            Order order = byId.get();
            order.setStatus(3);
            orderRepository.save(order);
        }
    }


}
