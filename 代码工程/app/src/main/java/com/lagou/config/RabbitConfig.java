package com.lagou.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class RabbitConfig {

    @Bean
    public Queue queue(){
        Map<String,Object> arguments = new HashMap<>();

        // 队列中消息的生存时间 10s
        arguments.put("x-message-ttl",10 * 1000);
        // 设置该队列所关联的死信交换器（当队列消息TTL到期后依然没有消费，则加入死信队列）
        arguments.put("x-dead-letter-exchange", "ex.pay.dlx");
        // 设置该队列所关联的死信交换器的routingKey，如果没有特殊指定，使用原队列的routingKey
        arguments.put("x-dead-letter-routing-key", "pay.dlx");
        return new Queue("q.pay",true,false,false,arguments);
    }

    // 死信队列
    @Bean
    public Queue queueDlx(){
        return new Queue("q.pay.dlx",true,false,false);
    }

    @Bean
    public Exchange exchange(){
        return new DirectExchange("ex.pay",true,false,null);
    }

    // 死信交换器
    @Bean
    public Exchange exchangeDlx(){
        return new DirectExchange("ex.pay.dlx",true,false,null);
    }



    @Bean
    public Binding binding(){
        return BindingBuilder.bind(queue()).to(exchange()).with("pay").noargs();
    }

    // 死信交换器绑定死信队列
    @Bean
    public Binding bindingDlx(){
        return BindingBuilder.bind(queueDlx()).to(exchangeDlx()).with("pay.dlx").noargs();
    }


}
